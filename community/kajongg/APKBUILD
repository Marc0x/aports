# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kajongg
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/applications/games/org.kde.kajongg"
pkgdesc="Mah Jongg - the ancient Chinese board game for 4 players"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	python3
	py3-twisted
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	libkmahjongg-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kajongg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
61e36b747790f34f0453fbc736b43728476b953ff636d014fb4ac083e5a0835f515db69be43286dc14d2fb5a2d5ad49afbddefd3a273abe738f58405687a5cdf  kajongg-22.12.0.tar.xz
"
