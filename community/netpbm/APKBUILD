# Contributor: Tom Parker-Shemilt <palfrey@tevp.net>
# Maintainer: Sodface <sod@sodface.com>
pkgname=netpbm
pkgver=11.00.02
_pkgver=11.0.2
pkgrel=1
pkgdesc="Toolkit for manipulation of graphic images"
url="https://netpbm.sourceforge.net/"
arch="all"
license="IJG AND BSD-3-Clause AND GPL-2.0-only"
checkdepends="diffutils"
makedepends="
	bash
	flex
	ghostscript
	libjpeg-turbo-dev
	libpng-dev
	libx11-dev
	libxml2-dev
	perl
	sed
	tiff-dev
	zlib-dev
	"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-extras::noarch"
source="https://github.com/ceamac/netpbm-make-dist/releases/download/v$_pkgver/netpbm-$_pkgver.tar.xz
	config.mk
	01-makefile.patch
	02-installnetpbm.patch
	"
builddir="$srcdir/$pkgname-$_pkgver"

build() {
	cat config.mk.in "$srcdir"/config.mk > config.mk

	make
	make package pkgdir="$PWD/pkgdir"
}

check() {
	# disable failing tests
	sed -i -e '/^pbmtext-iso88591.test/d' \
		-e '/^ilbm-roundtrip.test/d' \
		-e '/^ppmpat-random.test/d' "$builddir"/test/Test-Order

	make check pkgdir="$PWD/pkgdir"
}

package() {
	mkdir -p "$pkgdir"/usr/bin/ \
		"$pkgdir"/usr/lib/pkgconfig

	BUILDDIR="$builddir" PKGDIR="$pkgdir" ./installnetpbm

	# stray symlink named '*' because target dir does not exist
	rm "$pkgdir"/usr/include/\*

	sed -i -e "s/Version: Netpbm $pkgver/Version: $pkgver/" \
		-e "s|$pkgdir||g" "$pkgdir"/usr/lib/pkgconfig/netpbm.pc

	install -Dm644 -t "$pkgdir"/usr/share/man/man1 \
		"$builddir"/userguide/*.1
	install -Dm644 -t "$pkgdir"/usr/share/man/man3 \
		"$builddir"/userguide/*.3
	install -Dm644 -t "$pkgdir"/usr/share/man/man5 \
		"$builddir"/userguide/*.5

	install -Dm644 -t "$pkgdir"/usr/share/doc/$pkgname/html \
		"$builddir"/userguide/*.html

	install -Dm644 "$builddir"/doc/copyright_summary \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

extras() {
	pkgdesc="Netpbm perl scripts"
	depends="perl"

	amove usr/bin/pamfixtrunc
	amove usr/bin/pbmtox10bm
	amove usr/bin/pgmcrater
	amove usr/bin/pnmflip
	amove usr/bin/pnmquant
	amove usr/bin/pnmquantall
	amove usr/bin/ppmfade
	amove usr/bin/ppmquant
	amove usr/bin/ppmquantall
	amove usr/bin/ppmrainbow
	amove usr/bin/ppmshadow
}

sha512sums="
6b2e4833d1be30a1cc46071643b6acba95b3f5b251014ff2a910f4d03bd29f0ef096e4bacb31e502f77eeffeece3c90320ce55ba322b8ce669782d69f64c8ced  netpbm-11.0.2.tar.xz
7dc16e0ad3229fac35c3ddecdbfb43ebfb1a2b778e53ca34d5f45f738e15fd131298bc8faa6b9a428380ffa4f1c9b2101934b79fe37bb7e6c046910fcf8ac45c  config.mk
098f4270b20b585559fcd6ba088c5294ebfac02e912ed1f86577550c2b45bc7c61a22e9be1177116bdb7b21e51f0758b5612cd6eef2b0f03e8de06a69e241529  01-makefile.patch
0b3a67399b044e03800ffee4a1231ff5d3bc3c00939e609656eda8a34d460bf9959085e0adec5d9aae86676670d4001f5d3892391c2c54160c0fbe4dd3439881  02-installnetpbm.patch
"
