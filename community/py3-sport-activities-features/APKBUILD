# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-sport-activities-features
pkgver=0.3.7.2
pkgrel=0
pkgdesc="A minimalistic toolbox for extracting features from sport activity files"
url="https://github.com/firefly-cpp/sport-activities-features"
arch="noarch !ppc64le !s390x !riscv64" # py3-niaaml
license="MIT"
depends="
	python3
	py3-dotmap
	py3-geopy
	py3-geotiler
	py3-gpxpy
	py3-matplotlib
	py3-niaaml
	py3-overpy
	py3-requests
	py3-tcxreader
	"
checkdepends="python3-dev py3-pytest"
makedepends="py3-gpep517 py3-poetry-core"
subpackages="$pkgname-doc"
source="https://github.com/firefly-cpp/sport-activities-features/archive/$pkgver/sport-activities-features-$pkgver.tar.gz"
builddir="$srcdir/sport-activities-features-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m pytest -k "not test_weather"
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/sport_activities_features-$pkgver-py3-none-any.whl

	install -Dm644 docs/preprints/A_minimalistic_toolbox.pdf -t "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
52b80602b8475b071da959bd9132a973668366de7c1cbbef6f2fccebf9f7cb5214aa45a6c4d68a5c27f55d501c80546baf2f77d76a0cfe20fb5271db25ee7dc8  sport-activities-features-0.3.7.2.tar.gz
"
