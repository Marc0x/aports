# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaddressbook
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://apps.kde.org/kaddressbook/"
pkgdesc="Address Book application to manage your contacts"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends="kdepim-runtime"
makedepends="
	akonadi-dev
	akonadi-search-dev
	extra-cmake-modules
	gpgme-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kontactinterface-dev
	kpimtextedit-dev
	kuserfeedback-dev
	kuserfeedback-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	prison-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaddressbook-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a433c0e1aa5c9b8dd18bfbb010a9fc94ebd458b6a7848c14797d1fe30d44ca31553eef537e51bc6955cecb1d2aaee01850166dbe329ad749101e4e4837c97f91  kaddressbook-22.12.0.tar.xz
"
