# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=mailcommon
pkgver=22.12.0
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64" # Limited by messagelib -> qt5-qtwebengine
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
# TODO: Consider replacing gnupg with specific gnupg subpackages that mailcommon really needs.
depends="gnupg"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kmailtransport-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	mailimporter-dev
	messagelib-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	syntax-highlighting-dev
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
80d8b7f28f5ddf3ba5b42495e5d0096f6cfdb82e3cf8daa996c9bd412fe171ef295257b92855165a4f70858e377319d5722ea3c69816c6f2563c2710fb087ce7  mailcommon-22.12.0.tar.xz
"
