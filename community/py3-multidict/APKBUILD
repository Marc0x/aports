# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Contributor: Antoine Fontaine <antoine.fontaine@epfl.ch>
# Maintainer:
pkgname=py3-multidict
_pkgname=multidict
pkgver=6.0.3
pkgrel=0
pkgdesc="multidict implementation"
url="https://github.com/aio-libs/multidict/"
arch="all"
license="Apache-2.0"
depends="python3"
checkdepends="py3-pytest py3-pytest-cov"
makedepends="python3-dev py3-setuptools cython"
source="$_pkgname-$pkgver.tar.gz::https://github.com/aio-libs/multidict/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!strip"

check() {
	python3 -m pytest
}

build() {
	(cd multidict && find -name '*.pyx' -exec cython {} \;)
	python3 setup.py build
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
	rm -f "$pkgdir"/usr/lib/python3*/site-packages/*.c
	rm -f "$pkgdir"/usr/lib/python3*/site-packages/*.h
}

sha512sums="
95aef45378c0af6ef6b05549d7419ff4ba272430edae37b2fc041a4742d58890ba9475648b8b5ac57d2bff3718519a0201819b274a65ac0b101cf41c8abad812  multidict-6.0.3.tar.gz
"
