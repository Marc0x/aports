# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=mkvtoolnix
pkgver=72.0.0
pkgrel=0
pkgdesc="Set of tools to create, edit and inspect Matroska files"
url="https://mkvtoolnix.download/index.html"
# riscv64 blocked by fatal error: boost/core/use_default.hpp: No such file or directory
arch="all !riscv64"
license="GPL-2.0-only"
makedepends="
	boost-dev
	cmark-dev
	docbook-xsl
	file-dev
	flac-dev
	gmp-dev
	gtest-dev
	libmatroska-dev
	libogg-dev
	libvorbis-dev
	pcre2-dev
	qt6-qtbase-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg
	ruby
	ruby-rake
	zlib-dev
	"
subpackages="$pkgname-doc $pkgname-gui"
source="https://mkvtoolnix.download/sources/mkvtoolnix-$pkgver.tar.xz"

build() {
	./configure CC="${CC:-gcc}" CFLAGS="$CFLAGS" \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-gettext
	rake V=1 -j$JOBS
}

check() {
	rake V=1 -j$JOBS tests:unit
	rake V=1 -j$JOBS tests:run_unit
}

package() {
	rake DESTDIR="$pkgdir" install
}

gui() {
	pkgdesc="$pkgdesc (GUI)"
	depends="$pkgname qt6-qtsvg"

	amove usr/bin/mkvtoolnix-gui
	amove usr/share
}

sha512sums="
dc18c6e63e724181e38753921ec3374752dbb7c4d20650cf0787e9049aaec4f6bb6c744e49b3783921804f192de742fba92826615a1edc8516d2a41621be658c  mkvtoolnix-72.0.0.tar.xz
"
