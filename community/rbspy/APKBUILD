# Contributor: Adam Jensen <adam@acj.sh>
# Maintainer: Adam Jensen <adam@acj.sh>
pkgname=rbspy
pkgver=0.14.0
pkgrel=0
pkgdesc="Sampling profiler for Ruby"
url="https://rbspy.github.io/"
arch="all !armv7 !ppc64le !s390x !riscv64" # limited by cargo and build errors
license="MIT"
makedepends="cargo"
checkdepends="ruby"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/rbspy/rbspy/archive/v$pkgver.tar.gz"

build() {
	cargo build --release --locked
}

check() {
	# Some tests need additional privileges
	cargo test --release --locked -- \
		--skip sampler::tests \
		--skip test_current_thread_address \
		--skip test_initialize_with_disallowed_process \
		--skip test_get_exec_trace \
		--skip test_get_trace
}

package() {
	install -Dm755 "target/release/rbspy" "$pkgdir/usr/bin/rbspy"
}

sha512sums="
87707f8386435ca850ed30e3d40bb6082b1b8423d2930145cf0217624f7238d4d8bd477c81734986871660a9a3d031db8ded6f5f414a0a29dcfce1594f712bfc  rbspy-0.14.0.tar.gz
"
