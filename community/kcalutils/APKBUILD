# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalutils
pkgver=22.12.0
pkgrel=0
pkgdesc="The KDE calendar utility library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://api.kde.org/kdepim/kcalutils/html"
license="LGPL-2.0-or-later"
depends_dev="
	grantlee-dev
	kcalendarcore-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kwidgetsaddons-dev
	samurai
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalutils-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kcalutils-testincidenceformatter and kcalutils-testtodotooltip are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kcalutils-test(incidenceformatter|todotooltip|dndfactory)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dfd16296bcf029d03171b57d8c7cdd5781bc3f7d386e4ae321bd670978a837b3db60d44fad86e010716878c2fcb5124a62df25d45b45eb4ac7955e9c0c0e78f8  kcalutils-22.12.0.tar.xz
"
