# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkmahjongg
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games"
pkgdesc="Common code, backgrounds and tile sets for games using Mahjongg tiles"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkmahjongg-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests found

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2fbe15afb30ddbbe4d96c7a37f48f533caeef6f20f3f02113e6b4646cad3e65f281cb9090182503b1a8edc95115b20eb852371aea917e3841726cd0b1b08959e  libkmahjongg-22.12.0.tar.xz
"
