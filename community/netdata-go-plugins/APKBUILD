# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=netdata-go-plugins
pkgver=0.46.0
pkgrel=0
pkgdesc="netdata go.d.plugin"
url="https://github.com/netdata/go.d.plugin"
arch="all !x86 !armv7 !armhf" # checks fail
license="GPL-3.0-or-later"
depends="netdata"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://codeload.github.com/netdata/go.d.plugin/tar.gz/refs/tags/v$pkgver"
builddir="$srcdir/go.d.plugin-$pkgver"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o go.d.plugin ./cmd/godplugin
}

check() {
	go test ./...
}

package() {
	 mkdir -p "$pkgdir/usr/lib/netdata/conf.d"
	 cp -r "$builddir/config/go.d.conf" "$builddir/config/go.d" "$pkgdir/usr/lib/netdata/conf.d/"

	 mkdir -p "$pkgdir/usr/libexec/netdata/plugins.d/"
	 install -D -m755 -t "$pkgdir/usr/libexec/netdata/plugins.d" "$builddir/go.d.plugin"
}

sha512sums="
1d0fdd3acd45cc7cde1319530d9ae2463a3754dabfac0a6e8f91d2c4e4302c4c2bdc77205a0fb07a01e136f29690c670f74f1b5a82a326ca86f181e59d221c6b  netdata-go-plugins-0.46.0.tar.gz
"
