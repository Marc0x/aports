# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtshadertools
pkgver=6.4.1
pkgrel=0
pkgdesc="Experimental module providing APIs and a host tool to host tool to perform graphics and compute shader conditioning"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtshadertools-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtshadertools-everywhere-src-${pkgver/_/-}.tar.xz
	"

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3064a311601a2bbb3396e96ba5c27bb29b5c25e41cae7c4b736cc1e1e32ef6b5aa455ec8b0d3cb7b02f7472b133e4ef23627bdf2e60c188f07595edff15f7c87  qtshadertools-everywhere-src-6.4.1.tar.xz
"
