# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=incidenceeditor
pkgver=22.12.0
pkgrel=0
pkgdesc="KDE PIM incidence editor"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	calendarsupport-dev
	eventviews-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdiagram-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kldap-dev
	kmailtransport-dev
	kmime-dev
	libkdepim-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/incidenceeditor-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# akonadi-sqlite-incidencedatetimetest and akonadi-mysql-incidencedatetimetest require running DBus
	# ktimezonecomboboxtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(akonadi-(sqlite|mysql)-incidencedatetime|ktimezonecombobox)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
fecb30cc1533c07f73dd1d49459e9fe45186568d4e96eb505dab72fc3a5a2f45160dd276f778d6853d8d24ecee0686fa30232e9a0bb35270452f285d323d43a8  incidenceeditor-22.12.0.tar.xz
"
