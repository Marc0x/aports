# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwave
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kwave.sourceforge.net/"
pkgdesc="A sound editor for KDE"
license="GPL-2.0-or-later"
makedepends="
	alsa-lib-dev
	audiofile-dev
	extra-cmake-modules
	fftw-dev
	flac-dev
	imagemagick
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libsamplerate-dev
	libvorbis-dev
	opus-dev
	pulseaudio-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwave-$pkgver.tar.xz"
subpackages="$pkgname-libs $pkgname-doc $pkgname-lang"

build() {

	if [ "$CARCH" = s390x ]; then
		doc_opts="-DWITH_DOC=OFF"
	else
		doc_opts="-DWITH_DOC=ON"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWITH_OSS=OFF \
		-DWITH_MP3=OFF \
		$doc_opts
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0d7bc383750ed2a71e2dd45e01785729d954fa4adc80accfed35605c16fbdec7d54f045a266ee4b0d663f3c280d3c00409c82c03e587bba3396ee3f876c78a16  kwave-22.12.0.tar.xz
"
