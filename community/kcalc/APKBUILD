# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalc
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kcalc"
pkgdesc="Scientific Calculator"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	gmp-dev
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kinit-dev
	knotifications-dev
	kxmlgui-dev
	mpfr-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2546c9d348663d6eaa220e52d7062ab2956fbf8f76257fd4d9a0aa8fafc4c72609eff3e0a60eb67c8b4e5a0b5c99a3c50c0318f4cf8e1bc1aaff05813ddd7254  kcalc-22.12.0.tar.xz
"
