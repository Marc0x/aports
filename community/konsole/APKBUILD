# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=konsole
pkgver=22.12.0
pkgrel=1
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/applications/system/konsole"
pkgdesc="KDE's terminal emulator"
license="GPL-2.0-only AND LGPL-2.1-or-later AND Unicode-DFS-2016"
depends="ncurses-terminfo-base"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kpty-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="mesa-dri-gallium xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/konsole-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_PLUGIN_SSHMANAGER=ON
	cmake --build build
}

check() {
	cd build

	# DBusTest requires running DBus
	local skipped_tests="("
	local tests="
		DBus
		History
		"
	case "$CARCH" in
		armv7)
			tests="$tests Part"
			;;
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)Test"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8a967c0cfe59fde090b63d959428e35b0b96f08479f938009168691fdbcc76cd80d62dc77b884b5594337188017b784e321b00920ec6e03d27e1f5eb6741c3b1  konsole-22.12.0.tar.xz
"
