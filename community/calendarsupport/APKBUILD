# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=calendarsupport
pkgver=22.12.0
pkgrel=0
pkgdesc="Library providing calendar support"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org"
license="GPL-2.0-or-later AND Qt-GPL-exception-1.0 AND LGPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-notes-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kidentitymanagement-dev
	kio-dev
	kmime-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/calendarsupport-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
338e391311522d8cdc7846f206d3f7c9ddb71edb6e92b7db581ae0339b7358b73550ba8c287015fa5324a2bd010d526495c11989227885ed5f205a3c8af222ae  calendarsupport-22.12.0.tar.xz
"
