# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktorrent
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/internet/org.kde.ktorrent"
pkgdesc="A powerful BitTorrent client for KDE"
license="GPL-2.0-or-later"
makedepends="
	boost-dev
	extra-cmake-modules
	karchive-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kplotting-dev
	kross-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libktorrent-dev
	phonon-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	solid-dev
	syndication-dev
	taglib-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktorrent-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	# The infowidget plugin is disabled due to an incompatibility with musl
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_INFOWIDGET_PLUGIN=FALSE
	cmake --build build
}

check() {
	cd build

	# ipblocklisttest requires itself installed
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "ipblocklisttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e36c415a62a9e44cac9189ed000191ff557ee63bcfa0b7e587ac33e50d8d944dc1d2913b48e257113644555b01f5b8ed6e4320c89a42cc2b134034b947f5cd8f  ktorrent-22.12.0.tar.xz
"
