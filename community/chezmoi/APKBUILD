# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=chezmoi
pkgver=2.27.3
pkgrel=1
pkgdesc="Manage your dotfiles across multiple machines, securely."
url="https://www.chezmoi.io/"
arch="all"
license="MIT"
makedepends="go"
checkdepends="unzip"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/twpayne/chezmoi/archive/v$pkgver.tar.gz
	makefile-quote-built-by.patch
	"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make \
		VERSION="$pkgver" \
		DATE="$(date --utc +%Y-%m-%dT%H:%M:%SZ)" \
		BUILT_BY="Alpine Linux"
}

check() {
	make test
}

package() {
	make install \
		VERSION="$pkgver" \
		DATE="$(date --utc +%Y-%m-%dT%H:%M:%SZ)" \
		BUILT_BY="Alpine Linux" \
		DESTDIR="$pkgdir" \
		PREFIX="/usr"

	install -Dm0644 completions/chezmoi-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm0644 completions/chezmoi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
	install -Dm0644 completions/chezmoi.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/chezmoi"
	cp "$builddir/docs/"* "$subpkgdir/usr/share/doc/chezmoi"
}

sha512sums="
5068dddd8639674aadfe5b266817f8158d1a125f527aea21324f9a3cf17434cb91be77acb8846258ebec1f21d4c838dfbfe60b4c82ceba81f24464e776dc0661  chezmoi-2.27.3.tar.gz
6be6a032054e5d33ac74586c31381ab9332e8a22faff8ea2ff4248c4eddc3300243890c0e7d064db2648b336355115d597bf686aa70cea13b2250710ab885c9e  makefile-quote-built-by.patch
"
