# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmime
pkgver=22.12.0
pkgrel=0
pkgdesc="Library for handling mail messages and newsgroup articles"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kmime-headertest and kmime-messagetest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kmime-(header|message)test"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
12dc6e89e74e1c377acff410ae02d42ff0d31de0f56b079ddb6c34c42e80498d622ac4380be7990c5ea0034b1d292df12746f88c5063cb12320cef9b5d2d04d6  kmime-22.12.0.tar.xz
"
