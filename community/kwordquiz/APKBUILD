# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwordquiz
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kwordquiz"
pkgdesc="Flash Card Trainer"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkeduvocdocument-dev
	phonon-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwordquiz-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ce6f118108cc66d4ef89f745a7ad0554be1d6174a6c694bba1c003772da6ed8a03f42064f2a809ca566e270172202365dd6e0220fc1b2008eb60b2db6f17618e  kwordquiz-22.12.0.tar.xz
"
